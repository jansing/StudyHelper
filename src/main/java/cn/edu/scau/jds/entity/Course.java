package cn.edu.scau.jds.entity;

import cn.edu.scau.jds.enumeration.Season;

import java.time.Year;
import java.util.List;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-22.
 */
public class Course extends BaseEntity {

    private Year openYear;
    private Season openSeason;
    private String name;
    private String teacher;
    private String digest;
    private List<Material> materialList;
    private List<Job> jobList;
    private List<User> userList;

    public Course() {
    }

    public Year getOpenYear() {
        return openYear;
    }

    public void setOpenYear(Year openYear) {
        this.openYear = openYear;
    }

    public Season getOpenSeason() {
        return openSeason;
    }

    public void setOpenSeason(Season openSeason) {
        this.openSeason = openSeason;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public List<Material> getMaterialList() {
        return materialList;
    }

    public void setMaterialList(List<Material> materialList) {
        this.materialList = materialList;
    }

    public List<Job> getJobList() {
        return jobList;
    }

    public void setJobList(List<Job> jobList) {
        this.jobList = jobList;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
