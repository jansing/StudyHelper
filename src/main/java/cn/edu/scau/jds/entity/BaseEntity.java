package cn.edu.scau.jds.entity;

import java.io.Serializable;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-22.
 */
public class BaseEntity implements Serializable {

    private long id;
    private boolean valid = true;

    public BaseEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
