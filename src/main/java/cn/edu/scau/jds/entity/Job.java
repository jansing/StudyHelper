package cn.edu.scau.jds.entity;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
public class Job extends BaseEntity {

    private String name;
    private String digest;
    private String namingFormat;
    private LocalDateTime deadline;
    private String path;
    private List<Demand> demandList;
    // 不需要有Course，原因是进入Course之后才看得到Job

    public Job() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getNamingFormat() {
        return namingFormat;
    }

    public void setNamingFormat(String namingFormat) {
        this.namingFormat = namingFormat;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Demand> getDemandList() {
        return demandList;
    }

    public void setDemandList(List<Demand> demandList) {
        this.demandList = demandList;
    }
}
