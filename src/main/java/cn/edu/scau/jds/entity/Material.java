package cn.edu.scau.jds.entity;

import cn.edu.scau.jds.enumeration.MaterialType;

import java.time.LocalDateTime;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-22.
 */
public class Material extends BaseEntity {

    private String name;
    private String provider;
    private LocalDateTime createTime;
    private String path;
    private MaterialType type;
    // 不需要有Course，原因是要先进入Course之后才能查看Material，所以Course已经确定了

    public Material() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MaterialType getType() {
        return type;
    }

    public void setType(MaterialType type) {
        this.type = type;
    }
}
