package cn.edu.scau.jds.entity;

import cn.edu.scau.jds.enumeration.FileType;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
public class Demand {

    private String namingFormat;
    private FileType type;

    public Demand() {
    }

    public String getNamingFormat() {
        return namingFormat;
    }

    public void setNamingFormat(String namingFormat) {
        this.namingFormat = namingFormat;
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }
}
