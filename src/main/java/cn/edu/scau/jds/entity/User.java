package cn.edu.scau.jds.entity;

import java.util.List;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-22.
 */
public class User extends BaseEntity {

    private String sid;
    private String name;
    private String password;
    private String qq;
    private List<Course> courseList;

    public User() {
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }
}
