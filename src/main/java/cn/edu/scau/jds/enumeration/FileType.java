package cn.edu.scau.jds.enumeration;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
public enum FileType {
    TXT("txt", 1), DOCX("docx", 2), PPTX("pptx", 3), XLSX("xlsx", 4), PDF("pdf", 5), RAR("rar", 6);

    private String name;
    private int value;

    FileType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
