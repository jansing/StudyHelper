package cn.edu.scau.jds.enumeration;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
public enum MaterialType {

    COURSEWARE("课件", 1), HOMEWORK("作业", 2), EXAMINATION("考试", 3), EXPANSION("拓展", 4);

    private String name;
    private int value;

    MaterialType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
