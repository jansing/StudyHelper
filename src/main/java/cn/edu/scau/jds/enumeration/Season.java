package cn.edu.scau.jds.enumeration;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
public enum Season {

    SPRING("春季", 1), AUTUMN("秋季", 2);

    private String name;
    private int value;

    Season(String name, int vale) {
        this.name = name;
        this.value = vale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
