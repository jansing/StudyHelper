package cn.edu.scau.jds.enumeration;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
public enum Assessment {

    PERFECT("完美", 1), EXCELLENT("优秀", 2), GOOD("良好", 3), PASS("完成", 4), FAIL("未完成", 5);

    private String name;
    private int value;

    Assessment(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
