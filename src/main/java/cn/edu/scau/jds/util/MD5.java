package cn.edu.scau.jds.util;

import cn.edu.scau.jds.exception.JDSRuntimeException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 算法步骤：
 * 1、将String类型的密码转成byte数组
 * 2、用每个byte去和11111111做与运算并且得到的是int类型的值：byte & 11111111;(byte&0xff)
 * 3、把int 类型转成 16进制并返回String类型；
 * 4、返回的String如果不够两位，前面补0（不满八个二进制位就补全）；
 * 5：将每次返回的String并起来就是md5加密后的密码
 * <p>
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
public class MD5 {

    /**
     * 基础加密算法：进行一次MD5加密
     *
     * @param password 密码
     * @return MD5加密之后的密码
     * @throws NoSuchAlgorithmException
     */
    private static String fundamentalEncrypt(String password) throws NoSuchAlgorithmException {
        // 获取MD5算法
        MessageDigest md = MessageDigest.getInstance("md5");
        // 将密码拆分成数组
        byte[] pwdByteArray = md.digest(password.getBytes());
        StringBuilder result = new StringBuilder();
        for (byte b : pwdByteArray) {
            int number = b & 0XFF;
            String temp = Integer.toHexString(number);
            if (temp.length() == 1) {
                result.append("0");
            }
            result.append(temp);
        }
        return result.toString();
    }

    /**
     * 程序采用的加密算法
     *
     * @param password 密码
     * @return 二次加密之后的密码
     */
    public static String encrypt(String password) {
        try {
            return fundamentalEncrypt(fundamentalEncrypt(password));
        } catch (NoSuchAlgorithmException e) {
            throw new JDSRuntimeException("密码无法进行加密！加密算法不存在", e);
        }
    }

/*    public static String encrypt(String password, int times) throws NoSuchAlgorithmException {
        for (int i = 0; i < times; i++) {
            password = encrypt(password);
        }
        return password;
    }*/
}
