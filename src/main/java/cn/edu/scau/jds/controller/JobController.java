package cn.edu.scau.jds.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
@Controller(value = "JobController")
@RequestMapping(value = "/job")
public class JobController {
}
