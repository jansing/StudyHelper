package cn.edu.scau.jds.controller;

import cn.edu.scau.jds.entity.User;
import cn.edu.scau.jds.exception.JDSException;
import cn.edu.scau.jds.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
@Controller(value = "userController")
@RequestMapping(value = "/user")
public class UserController {

    @Resource(name = "userService")
    private UserService userService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("user", new User());
        return "user/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(User user) {
        try {
            userService.add(user);
        } catch (JDSException e) {
            // TODO 返回注册页面，提示该用户已经被注册
        }
        return "user/result";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String display(@PathVariable(value = "id") long id, Model model) {
        model.addAttribute("user", userService.load(id));
        return "user/display";
    }
}
