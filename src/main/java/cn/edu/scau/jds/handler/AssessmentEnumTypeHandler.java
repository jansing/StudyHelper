package cn.edu.scau.jds.handler;

import cn.edu.scau.jds.enumeration.Assessment;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
@MappedTypes(value = Assessment.class)
@MappedJdbcTypes(value = JdbcType.VARCHAR)
public class AssessmentEnumTypeHandler extends BaseTypeHandler<Assessment> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Assessment parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter.getName());
    }

    @Override
    public Assessment getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return transfer(rs.getString(columnName));
    }

    @Override
    public Assessment getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return transfer(rs.getString(columnIndex));
    }

    @Override
    public Assessment getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return transfer(cs.getString(columnIndex));
    }

    private Assessment transfer(String name) {
        for (Assessment assessment : Assessment.values()) {
            if (name.equals(assessment.getName()))
                return assessment;
        }
        throw new IllegalArgumentException("非法的枚举值！请检查是否存在名为" + name + "的Assessment枚举");
    }
}
