package cn.edu.scau.jds.handler;

import cn.edu.scau.jds.enumeration.MaterialType;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
@MappedTypes(value = MaterialType.class)
@MappedJdbcTypes(value = JdbcType.VARCHAR)
public class MaterialTypeEnumTypeHandler extends BaseTypeHandler<MaterialType> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, MaterialType parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter.getName());
    }

    @Override
    public MaterialType getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return transfer(rs.getString(columnName));
    }

    @Override
    public MaterialType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return transfer(rs.getString(columnIndex));
    }

    @Override
    public MaterialType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return transfer(cs.getString(columnIndex));
    }

    private MaterialType transfer(String name) {
        for (MaterialType materialType : MaterialType.values()) {
            if (name.equals(materialType.getName()))
                return materialType;
        }
        throw new IllegalArgumentException("非法的枚举值！请检查是否存在名为" + name + "的MaterialType枚举");
    }
}
