package cn.edu.scau.jds.handler;

import cn.edu.scau.jds.enumeration.FileType;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
@MappedTypes(value = FileType.class)
@MappedJdbcTypes(value = JdbcType.VARCHAR)
public class FileTypeEnumTypeHandler extends BaseTypeHandler<FileType> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, FileType parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter.getName());
    }

    @Override
    public FileType getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return transfer(rs.getString(columnName));
    }

    @Override
    public FileType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return transfer(rs.getString(columnIndex));
    }

    @Override
    public FileType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return transfer(cs.getString(columnIndex));
    }

    private FileType transfer(String name) {
        for (FileType fileType : FileType.values()) {
            if (name.equals(fileType.getName()))
                return fileType;
        }
        throw new IllegalArgumentException("非法的枚举值！请检查是否存在名为" + name + "的FileType枚举");
    }
}
