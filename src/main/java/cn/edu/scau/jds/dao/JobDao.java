package cn.edu.scau.jds.dao;

import cn.edu.scau.jds.entity.Job;
import org.springframework.stereotype.Repository;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
@Repository(value = "jobDao")
public class JobDao extends BaseDao<Job> {

}
