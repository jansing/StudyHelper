package cn.edu.scau.jds.dao;

import cn.edu.scau.jds.entity.Course;
import org.springframework.stereotype.Repository;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
@Repository(value = "courseDao")
public class CourseDao extends BaseDao<Course> {

}
