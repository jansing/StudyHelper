package cn.edu.scau.jds.dao;

import cn.edu.scau.jds.entity.Material;
import org.springframework.stereotype.Repository;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
@Repository(value = "materialDao")
public class MaterialDao extends BaseDao<Material> {

}
