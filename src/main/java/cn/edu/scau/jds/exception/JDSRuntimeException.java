package cn.edu.scau.jds.exception;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-02-02.
 */
public class JDSRuntimeException extends RuntimeException {

    public JDSRuntimeException() {
    }

    public JDSRuntimeException(String message) {
        super(message);
    }

    public JDSRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public JDSRuntimeException(Throwable cause) {
        super(cause);
    }
}
