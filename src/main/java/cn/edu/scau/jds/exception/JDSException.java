package cn.edu.scau.jds.exception;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-02-02.
 */
public class JDSException extends Exception {

    public JDSException() {
    }

    public JDSException(String message) {
        super(message);
    }

    public JDSException(String message, Throwable cause) {
        super(message, cause);
    }

    public JDSException(Throwable cause) {
        super(cause);
    }
}
