package cn.edu.scau.jds.service;

import org.apache.log4j.Logger;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
public class BaseService {

    protected final Logger logger = Logger.getLogger(this.getClass());

}
