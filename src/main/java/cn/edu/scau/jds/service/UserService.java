package cn.edu.scau.jds.service;

import cn.edu.scau.jds.dao.CourseDao;
import cn.edu.scau.jds.dao.UserDao;
import cn.edu.scau.jds.entity.Course;
import cn.edu.scau.jds.entity.User;
import cn.edu.scau.jds.exception.JDSException;
import cn.edu.scau.jds.util.MD5;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by junbin(seraphstorm@163.com) on 2016-01-26.
 */
@Service(value = "userService")
public class UserService extends BaseService {

    @Resource(name = "userDao")
    private UserDao userDao;
    @Resource(name = "courseDao")
    private CourseDao courseDao;

    public void add(User user) throws JDSException {
        if (userDao.loadBySid(user.getSid()) != null) {
            logger.error("试图添加已经存在的用户[sid：" + user.getSid() + "]");
            throw new JDSException("该用户已经存在！");
        } else {
            user.setPassword(MD5.encrypt(user.getPassword()));
            userDao.add(user);
            logger.info("添加用户[sid：" + user.getSid() + "]成功");
        }
    }

    public void delete(long id) throws JDSException {
        if (userDao.load(id) == null) {
            logger.error("试图删除不存在的用户[id：" + id + "]");
            throw new JDSException("用户不存在！无法删除！");
        }
        userDao.deleteUserCourse(id);
        logger.info("删除用户[id：" + id + "]的所有作业信息成功");
        userDao.deleteUserJob(id);
        logger.info("删除用户[id：" + id + "]的所有课程信息成功");
        userDao.deleteUser(id);
        logger.info("删除用户[id：" + id + "]成功");
    }

    public void updatePassword(User user) {
        if (!userDao.isExists(user)) {
            logger.error("要修改的用户[id：" + user.getId() + "]不存在");
        } else {
            user.setPassword(MD5.encrypt(user.getPassword()));
            userDao.updatePassword(user);
            logger.info("更新用户[id：" + user.getId() + "]密码成功");
        }
    }

    public void updateQq(User user) {
        if (!userDao.isExists(user)) {
            logger.error("要修改的用户[id：" + user.getId() + "]不存在");
        } else {
            userDao.updateQq(user);
            logger.info("更新用户[id：" + user.getId() + "]关联QQ成功");
        }
    }

    /**
     * 使用懒加载加载用户
     *
     * @param id 用户id
     * @return 用户
     */
    public User load(long id) {
        User user = userDao.load(id);
        if (user == null) {
            logger.error("要加载的用户不存在[id：" + id + "]");
        } else {
            logger.info("加载用户[id：" + id + "]成功");
        }
        return user;
    }

    /**
     * 使用积极加载加载用户
     *
     * @param id 用户id
     * @return 用户
     */
    public User aggressiveLoad(long id) {
        User user = userDao.aggressiveLoad(id);
        if (user == null) {
            logger.error("要加载的用户不存在[id：" + id + "]");
        } else {
            logger.info("加载用户[id：" + id + "]成功");
        }
        return user;
    }

    public User login(String sid, String password) throws JDSException {
        User user = userDao.loadBySid(sid);
        if (user == null) {
            logger.error("用户[sid：" + sid + "]不存在！");
            throw new JDSException("用户不存在或密码不正确");
        }
        if (!MD5.encrypt(password).equals(user.getPassword())) {
            logger.error("用户[sid：" + sid + "]密码不正确！");
            throw new JDSException("用户不存在或密码不正确");
        }
        logger.info("用户[sid：" + sid + "]登陆成功");
        return user;
    }

    public void addCourse(User user, Course course) throws JDSException {
        judge(user, course);
        if (user.getCourseList().stream().filter(c -> c.getId() == course.getId()).count() > 0) {
            logger.warn("该用户[id：" + user.getId() + "]已经参加了该门课程[id：" + course.getId() + "]，所以无法为其添加该课程");
            throw new JDSException("无需再添加该课程！");
        }
        userDao.addCourse(user, course);
        logger.info("为用户[id：" + user.getId() + "]添加课程[id：" + course.getId() + "]成功");
    }

    public void deleteCourse(User user, Course course) throws JDSException {
        judge(user, course);
        if (user.getCourseList().stream().filter(c -> c.getId() == course.getId()).count() != 1) {
            logger.warn("该用户[id：" + user.getId() + "]并未参加该门课程[id：" + course.getId() + "]，所以无法为其删除该课程");
            throw new JDSException("该用户没有参加该课程，所以无法删除该课程！");
        }
        userDao.deleteCourse(user, course);
        logger.info("为用户[id：" + user.getId() + "]删除课程[id：" + course.getId() + "]成功");
    }

    private void judge(User user, Course course) throws JDSException {
        if (!userDao.isExists(user)) {
            logger.error("用户[id：" + user.getId() + "]不存在，无法为其添加课程");
            throw new JDSException("用户不存在！");
        }
        if (!courseDao.isExists(course)) {
            logger.error("课程[id：" + course.getId() + "]不存在，无法被添加到用户");
            throw new JDSException("课程不存在！");
        }
    }

}
