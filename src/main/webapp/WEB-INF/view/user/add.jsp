<%--
  Created by IntelliJ IDEA.
  User: serap
  Date: 2016-01-26
  Time: 13:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>添加用户</title>
</head>
<body>
<sf:form method="post" modelAttribute="user">
    学号：<sf:input path="sid"/><br/>
    姓名：<sf:input path="name"/><br/>
    密码：<sf:input path="password"/><br/>
    关联QQ：<sf:input path="qq"/><br/>
    <input type="submit" value="注册">
</sf:form>
</body>
</html>