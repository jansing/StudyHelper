DROP DATABASE IF EXISTS jds;

CREATE DATABASE jds
  CHARACTER SET utf8;

USE jds;

DROP TABLE IF EXISTS user_job;
DROP TABLE IF EXISTS user_course;
DROP TABLE IF EXISTS demand;
DROP TABLE IF EXISTS job;
DROP TABLE IF EXISTS material;
DROP TABLE IF EXISTS course;
DROP TABLE IF EXISTS user;

CREATE TABLE user (
  id       BIGINT      NOT NULL PRIMARY KEY AUTO_INCREMENT, -- 主键
  sid      VARCHAR(12) NOT NULL, -- 学号
  name     VARCHAR(50) NOT NULL, -- 姓名
  password VARCHAR(50) NOT NULL             DEFAULT md5(md5('123456')), -- 密码
  qq       VARCHAR(20), -- 关联QQ
  is_valid TINYINT     NOT NULL             DEFAULT 1 -- 是否有效【由TypeHandler进行转换】
);

CREATE INDEX idx_user_sid ON user (sid);
CREATE INDEX idx_user_qq ON user (qq);
CREATE INDEX idx_user_is_valid ON user (is_valid);

CREATE TABLE course (
  id          BIGINT       NOT NULL PRIMARY KEY AUTO_INCREMENT, -- 主键
  open_year   YEAR         NOT NULL, -- 开设年份【由TypeHandler进行转换】
  open_season VARCHAR(6)   NOT NULL, -- 春季或者秋季【枚举值，由TypeHandler进行转换】
  name        VARCHAR(255) NOT NULL, -- 课程名称
  teacher     VARCHAR(50)  NOT NULL, -- 课程授师
  digest      VARCHAR(500) NOT NULL, -- 课程简介
  is_valid    TINYINT      NOT NULL             DEFAULT 1 -- 是否有效【由TypeHandler进行转换】
);

CREATE INDEX idx_course_open_year ON course (open_year);
CREATE INDEX idx_course_open_year_season ON course (open_year, open_season);
CREATE INDEX idx_course_teacher ON course (teacher);
CREATE INDEX idx_course_is_valid ON course (is_valid);

CREATE TABLE user_course (
  user_id   BIGINT NOT NULL,
  course_id BIGINT NOT NULL
);

CREATE INDEX idx_uc_user_id ON user_course (user_id);
CREATE INDEX idx_uc_course_id ON user_course (course_id);
CREATE UNIQUE INDEX unique_user_course ON user_course (user_id, course_id);

CREATE TABLE material (
  id          BIGINT       NOT NULL PRIMARY KEY AUTO_INCREMENT, -- 主键
  name        VARCHAR(255) NOT NULL, -- 资料名称
  provider    VARCHAR(50)  NOT NULL, -- 提供者名称
  create_time TIMESTAMP    NOT NULL, -- 贡献时间戳
  path        VARCHAR(255) NOT NULL, -- 本地磁盘资料存储路径
  type        VARCHAR(20)  NOT NULL, -- 资料类型【枚举值，由TypeHandler进行转换】
  course_id   BIGINT       NOT NULL,
  is_valid    TINYINT      NOT NULL             DEFAULT 1 -- 是否有效【由TypeHandler进行转换】
);

CREATE INDEX idx_material_provider ON material (provider);
CREATE INDEX idx_material_course_id ON material (course_id);
CREATE INDEX idx_material_is_valid ON material (is_valid);

CREATE TABLE job (
  id            BIGINT       NOT NULL PRIMARY KEY AUTO_INCREMENT, -- 主键
  name          VARCHAR(255) NOT NULL, -- 作业名称
  digest        VARCHAR(500) NOT NULL, -- 作业介绍
  naming_format VARCHAR(255) NOT NULL, -- 作业文件夹命名格式
  deadline      TIMESTAMP    NOT NULL, -- 截至日期
  path          VARCHAR(255) NOT NULL, -- 本地磁盘作业目录存储路径
  course_id     BIGINT       NOT NULL,
  is_valid      TINYINT      NOT NULL             DEFAULT 1 -- 是否有效【由TypeHandler进行转换】
);

CREATE INDEX idx_job_course_id ON job (course_id);
CREATE INDEX idx_job_is_valid ON job (is_valid);

CREATE TABLE demand (
  -- 作业要求一般有多个，而且隶属于一个作业，所以它应该不需要有主键字段，直接由作业持有一个List，作为一个属性使用
  -- id bigint not null primary key auto_increment, -- 主键
  naming_format VARCHAR(255) NOT NULL, -- 作业提交文件命名格式
  type          VARCHAR(10)  NOT NULL, -- 作业提交文件类型
  job_id        BIGINT       NOT NULL
  -- is_valid tinyint not null default 1 -- 是否有效【由TypeHandler进行转换】
);

CREATE INDEX idx_demand_job_id ON demand (job_id);

CREATE TABLE user_job (-- 只有插入、修改和查询，其中插入由触发器完成，所以实际只有修改和查询由Java完成
  user_id        BIGINT  NOT NULL,
  job_id         BIGINT  NOT NULL,
  path           VARCHAR(255), -- 该学生对应作业的本地磁盘存储路径
  assessment     VARCHAR(50), -- 自我评估【枚举值，由TypeHandler进行转换】
  is_submitted   TINYINT NOT NULL DEFAULT 0, -- 是否已提交
  submitted_time TIMESTAMP
);

CREATE INDEX idx_uj_user_id ON user_job (user_id);
CREATE INDEX idx_uj_job_id ON user_job (job_id);
CREATE INDEX idx_uj_is_submitted ON user_job (is_submitted);
CREATE UNIQUE INDEX unique_user_job ON user_job (user_id, job_id);

DELIMITER $$
CREATE TRIGGER user_job_trigger -- 学生_作业触发器
AFTER
INSERT
ON job
FOR EACH ROW
  BEGIN
    DECLARE total BIGINT;
    -- 某门课程的学生人数
    DECLARE i BIGINT DEFAULT 1;
    DECLARE uid BIGINT;
    -- 用户id

    CREATE TEMPORARY TABLE temp (
      id      BIGINT PRIMARY KEY AUTO_INCREMENT,
      user_id BIGINT
    );
    -- 临时表、存储某门课程的所有用户id
    INSERT INTO temp (user_id) SELECT user_id
                               FROM user_course
                               WHERE course_id = new.course_id;
    -- 不可以加as

    SET total = (SELECT count(id)
                 FROM temp);
    -- 获取课程的学生总数
    WHILE i <= total DO -- 将学生与对应布置的作业联系起来，循环插入数据到学生作业表中
      SET uid = (SELECT user_id
                 FROM temp
                 WHERE id = i);
      -- 先获取用户id
      INSERT INTO user_job (user_id, job_id) VALUE (uid, new.id);
      -- 插入数据
      SET i = i + 1;
    END WHILE;
    DROP TEMPORARY TABLE temp;
    -- 必须删除临时表，原因是有可能会造成重复创建表【跟临时表的生命周期有关】
  END$$
DELIMITER ;

-- 密码为12345654321
INSERT INTO user (sid, name, password) VALUE ('201330330229', '钟俊滨', '7144282aa136c498fdfb2526130147b4'),
  ('201330260121', '林淼霖', '7144282aa136c498fdfb2526130147b4'),
  ('201330260215', '孙琳徕', '7144282aa136c498fdfb2526130147b4'),
  ('201330330121', '周天琦', '7144282aa136c498fdfb2526130147b4'),
  ('201330120211', '邱立业', '7144282aa136c498fdfb2526130147b4'),
  ('201330330430', '白秋婕', '7144282aa136c498fdfb2526130147b4'),
  ('201335210111', '青诗诗', '7144282aa136c498fdfb2526130147b4'),
  ('201331520222', '莫晴', '7144282aa136c498fdfb2526130147b4');

INSERT INTO course (open_year, open_season, name, teacher, digest)
  VALUE ('2015', '秋季', '软件工程', '朱超', '软件工程是一门研究用工程化方法构建和维护有效的、实用的和高质量的软件的学科。它涉及程序设计语言、数据库、软件开发工具、系统平台、标准、设计模式等方面。'),
  ('2015', '秋季', '数据库系统概论', '司徒浩臻', '数据库领域的殿堂级作品，夯实数据库理论基础，增强数据库技术内功的必备之选，对深入理解数据库，深入研究数据库，深入操作数据库都具有极强的指导作用！');

INSERT INTO user_course
VALUES (1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (7, 1), (1, 2), (3, 2), (5, 2), (6, 2), (7, 2), (8, 2);

INSERT INTO job (name, digest, naming_format, deadline, path, course_id)
  VALUE ('软件神话', '软件神话XXX', '学号_姓名', '2015-10-11 23:59:59', '/job/软件工程/第1次作业：软件神话/', 1),
  ('综合性实验', '自行选择实验题目，做好分析与设计，完成数据库设计...', '学号+姓名', '2015-12-30 23:59:59', '/job/数据库系统概论/综合性实验/', 2);

-- 插入作业的时候触发器就会往学生-作业表插入新数据

INSERT INTO demand VALUES ('学号_姓名', 'DOC', 1), ('学号_姓名', 'DOC', 2), ('学号_姓名', 'RAR', 2);